function data = dm_randomwalk(N,runs)
// Random Walk
// Calling Sequence
// data = dm_randomwalk(N,runs)
// Parameters
// N:
// runs:
// data:
// 
// Creates some test data.
//(c)Eamonn Keogh and Abdullah Mueen
data(N*runs)  =  1;
 
 
for i = 1 : runs
    
    data(i) = 0; 
    p = 0;
    for j = 2 : N
       
        c = p + rand(1,1,"normal");
        data((j-1)*runs+i) = c;
        p = c;
    end;
      
end;
data = matrix(data,runs,N);
endfunction